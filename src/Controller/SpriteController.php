<?php

namespace Drupal\svg_spriter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Controller routines for admin block routes.
 */
class SpriteController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The Drupal file system.
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Retrieves the Drupal file system.
   *
   * @return \Drupal\Core\File\FileSystemInterface
   *   The Drupal file system.
   */
  protected function fileSystem() {
    if (!isset($this->fileSystem)) {
      $this->fileSystem = $this->container()->get('file_system');
    }
    return $this->fileSystem;
  }

  protected function createSprite($group) {
    $files = $this->discoverSvgFiles($group);
  // XXX Create sprite
    return '';
  }

  protected function discoverSvgFiles($group) {
    $files = [];
    foreach ($this->moduleHandler()->getModuleList() as $key => $value) {
      $module_path = $this->moduleHandler()->getModule($key)->getPath();
      $scan_dir = "$module_path/svg_spriter/$group";
      if (is_dir($scan_dir) && $scan = $this->fileSystem()->scanDirectory("$module_path/svg_spriter/$group", "/[a-z0-9_-]+.svg$/i", ['key'=> 'filename'])) {
        $files = array_merge($files, $scan);
      }
    }
    return $files;
  }

  public function getSprite($group = 'default') {
    $group = substr($group, 0, strpos($group, '.svg'));

    $sprite = $this->createSprite($group);

    $file = __DIR__ . '/../../README.md';
    $response = new BinaryFileResponse($file);
    return $response;

  }


  /**
   * Returns the service container.
   *
   * This method is marked private to prevent sub-classes from retrieving
   * services from the container through it. Instead,
   * \Drupal\Core\DependencyInjection\ContainerInjectionInterface should be used
   * for injecting services.
   *
   * @return \Symfony\Component\DependencyInjection\ContainerInterface
   *   The service container.
   */
  private function container() {
    return \Drupal::getContainer();
  }
}
